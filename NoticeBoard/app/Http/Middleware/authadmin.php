<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class authadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()){
            return redirect()->route('loginpage');
        }
        if(Auth::user()->isadmin==0){
            return $next($request); 
        }
        if(Auth::user()->isadmin==1){
            return redirect('user'); 
        }
    }
}
