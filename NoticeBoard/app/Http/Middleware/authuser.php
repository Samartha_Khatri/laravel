<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class authuser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()){
            return redirect()->route('loginpage');
        }
        if(Auth::user()->isadmin==0){
            return redirect('adminhome'); 
        }
        if(Auth::user()->isadmin==1){
            return $next($request);
        }
    }
}
