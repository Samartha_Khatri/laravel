<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\user;
use App\models\groups;
use App\models\noticemanagers;


class admincontroller extends Controller
{
    //Login_Logout Start
    function login(){
        return view('byadmin.login');
    }

    
    function logout(Request $r){
    	$r->session()->flush();
    	return redirect('admin/login');
    }

    //admin Home start
    function adminhome(Request $request){
        // $session_data = $request->session()->get('id');
        // $se= $session_data[0]->id;
        $users = user::where('trash','0')->get();
        return view('byadmin.home',['users'=>$users]);
    }
    function adminregister(Request $request){
        user::insert([
            'first_name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password)
        ]);
        return redirect('admin/home/users')->with('report','Registered Successfull!!');;
    }
    function normalregister(Request $request){
        user::insert([
            'isadmin'=>$request->isadmin,
            'first_name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password)
        ]);
        return redirect('admin/home/users')->with('report','Registered Successfull!!');;
    }
    function edithome($id){
        $edit= user::where('id',$id)->first();
        return view('byadmin.edithome',['edit'=>$edit]);
    }
    function updatehome(Request $request){
        user::where('id',$request->id)->update([
            'isadmin'=>$request->isadmin,
            'first_name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password)
        ]);
        return redirect(route('adminhome'))->with('report','update successfull!!!');
    }
    function delete($id){
        $trash = user::find($id);
        $trash->trash = 1;
        $trash->save();
        return back()->with('report','deleted successfull!!');
    }
    function hometrash(){
        $users = user::where('trash','1')->get();
        return view('byadmin.hometrash',['users'=>$users]);
    }
    function deletefromhometrash($id){
        user::where('id',$id)->delete();
        return back()->with('report','deleted successfull!!');
    }
    function restorefromhometrash($id){
        $trash = user::where('id',$id)->update(['trash' => 0]);
        return back()->with('report','Restored successfull!!');
    }
    //Admin End

    //Group Start
    function group(){
        $group = groups::where('Trash','0')->get();
        return view('byadmin.group',['group'=>$group]);
    }
    function addgroup(Request $request){
        groups::insert([
            'group_name'=>$request->groupname,
        ]);
        return back()->with('report','added group Successfull!!');
    }
    function editgroup($id){
        $edit= groups::where('group_id',$id)->first();
        $group = groups::where('Trash','0')->get();
        return view('byadmin.editgroup',['edit'=>$edit,'group'=>$group]);
    }
    function updategroup(Request $request){
        groups::where('group_id',$request->groupid)->update([
            'group_name'=>$request->groupname
        ]);
        return redirect(route('group.post'))->with('report','update successfull!!!');
    }
    function deletegroup($id){
        $trash = groups::where('group_id',$id)->update(['trash' => 1]);
        return back()->with('report','deleted successfull!!');
    }
    function deletefromgrouptrash($id){
        groups::where('group_id',$id)->delete();
        return back()->with('report','deleted successfull!!');
    }
    function grouptrash(){
        $group = groups::where('trash','1')->get();
        return view('byadmin.grouptrash',['group'=>$group]);
    }
    function restorefromgrouptrash($id){
        $trash = groups::where('group_id',$id)->update(['trash' => 0]);
        return back()->with('report','Restored successfull!!');
    }
    //Group End
    
    //Notice Start
    function listnoticeall(){
        $result=noticemanagers::with('groups')->where('trash','0')->get();
        return view('byadmin.listnoticeall',['result'=>$result]);     
    }
    function listnoticeexam(){
        $result=noticemanagers::with('groups')->where('trash','0')
        ->where('group_id','1')->get();
        return view('byadmin.listnoticeexam',['result'=>$result]);     
    }
    function listnoticevacation(){
        $result=noticemanagers::with('groups')->where('group_id','2')->get();
        // dd($result);
        return view('byadmin.listnoticevacation',['result'=>$result]);     
    }
    function listnoticeeca(){
        $result=noticemanagers::with('groups')->where('group_id','3')->get();
        return view('byadmin.listnoticeeca',['result'=>$result]);     
    }
    function addnotice(){
        $group = groups::all('group_id','group_name');
        return view('byadmin.addnotice',['result'=>$group]);     
    }
    function insertnotice(Request $request){
        $session_data = $request->session()->get('id');
        noticemanagers::insert([
            'group_id'=>$request->group,
            'details'=>$request->details,    
            'class'=>$request->class,
            // 'updated_by'=> $session_data[0]->id
        ]);
        $request->session()->flash('report','Registered Successfull!!');
        return redirect('admin/home/users');   
    }
    function editnotice($id){
        $result=groups::All();
        $edit= noticemanagers::where('notice_id',$id)->first();
        return view('byadmin.editnotice',['edit'=>$edit,'result'=>$result]);
    }
    function updatenotice(Request $request){
        $session_data = $request->session()->get('id');
        noticemanagers::where('notice_id',$request->notice_id)->update([
            'group_id'=>$request->group,
            'details'=>$request->details,    
            'class'=>$request->class,
        ]);
        return redirect(route('noticehome'))->with('report','update successfull!!!');
    }
    function deletenotice($id){
        $trash = noticemanagers::where('notice_id',$id)->update(['trash' => 1]);
        return back()->with('report','deleted successfull!!');
    }
    function deletefromnoticetrash($id){
        noticemanagers::where('notice_id',$id)->delete();
        return back()->with('report','deleted successfull!!');
    }
    function noticetrash(){
        $notice = noticemanagers::with('groups')->where('trash','1')->get();
        return view('byadmin.noticetrash',['notice'=>$notice]);
    }
    function restorefromnoticetrash($id){
        $trash = noticemanagers::where('notice_id',$id)->update(['trash' => 0]);
        return back()->with('report','Restored successfull!!');
    }
    //Notice End    
}
