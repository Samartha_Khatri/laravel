<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class noticemanagers extends Model
{
    use HasFactory;
    public function groups(){
        return $this->belongsTo(groups::class,'group_id','group_id');
    }
    
}
