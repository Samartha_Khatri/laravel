<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\adminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::view('admin/login','byadmin.login')->name('loginpage');
// Route::post('admin/log_verify',[adminController::class, 'login_verify'])->name('login.post');

Route::get('user',[adminController::class, 'userhome'])->name('userhome')->middleware('authuser');

Route::group(['middleware'=>['adminauth']],function(){
    Route::get('admin/home/users',[adminController::class, 'adminhome'])->name('adminhome');

//home

Route::view('admin/home/registernormaluser','byadmin.addnormaluser');
Route::view('admin/home/registeradminuser','byadmin.addadminuser');

Route::get('admin/home/trash',[adminController::class,'hometrash'])->name('hometrash');
Route::get('admin/home/trash/delete/{id}',[adminController::class,'deletefromhometrash']);
Route::get('admin/home/trash/restore/{id}',[adminController::class,'restorefromhometrash']);

//home CURD
Route::get('admin/home/edit/{id}',[adminController::class, 'edithome'])->name('edit');
Route::post('admin/home/update',[adminController::class, 'updatehome'])->name('updatehomeuser');
Route::get('admin/home/delete/{id}',[adminController::class, 'delete'])->name('delete.post');
Route::post('admin/adminregistrations_verify',[adminController::class, 'adminregister'])->name('adminregister.post');
Route::post('admin/normalregistrations_verify',[adminController::class, 'normalregister'])->name('normalregister');
//Home end

//Group
Route::get('admin/group',[adminController::class, 'group'])->name('group.post');
Route::post('admin/group_add',[adminController::class, 'addgroup'])->name('addgroup.post');
Route::get('admin/group_delete/{id}',[adminController::class, 'deletegroup']);

Route::get('admin/group/edit/{id}',[adminController::class, 'editgroup']);
Route::post('admin/group/update',[adminController::class, 'updategroup'])->name('updategroup');

Route::get('admin/group/trash',[adminController::class,'grouptrash'])->name('grouptrash');
Route::get('admin/group/trash/delete/{id}',[adminController::class,'deletefromgrouptrash']);
Route::get('admin/group/trash/restore/{id}',[adminController::class,'restorefromgrouptrash']);

//Groupend

//Notice
Route::get('admin/notice/all',[adminController::class, 'listnoticeall'])->name('noticehome');
Route::get('admin/notice/exam',[adminController::class, 'listnoticeexam']);
Route::get('admin/notice/vacation',[adminController::class, 'listnoticevacation']);
Route::get('admin/notice/eca',[adminController::class, 'listnoticeeca']);
Route::get('admin/notice/addnotice',[adminController::class, 'addnotice']);
//Notice Curd
Route::get('admin/notice/edit/{id}',[adminController::class, 'editnotice']);
Route::post('admin/notice/update',[adminController::class, 'updatenotice'])->name('updatenotice');
Route::get('admin/notice/delete/{id}',[adminController::class, 'deletenotice'])->name('deletenotice');

Route::post('admin/notice/addnotice_verify',[adminController::class, 'insertnotice'])->name('addnotice.post');

Route::get('admin/notice/trash',[adminController::class,'noticetrash'])->name('noticetrash');
Route::get('admin/notice/trash/delete/{id}',[adminController::class, 'deletefromnoticetrash']);
Route::get('admin/notice/trash/restore/{id}',[adminController::class,'restorefromnoticetrash']);

//Notice end



Route::get('admin/logout',[adminController::class, 'logout'])->name('logout.post');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});