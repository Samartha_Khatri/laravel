<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticemanagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticemanagers', function (Blueprint $table) {
            $table->id('notice_id');
            $table->biginteger('group_id')->unsigned();
            $table->foreign('group_id')->references('group_id')->on('groups');
            $table->string('details',500);
            $table->string('class');
            $table->boolean('Trash')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticemanagers');
        
    }
}
