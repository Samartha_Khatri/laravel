<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/index.css')}} ">
    <link rel="stylesheet" type="text/css" href="{{asset('asset/css/home.css')}}"> 

</head>
<body>
<div id="box">
  <div class="box-1">
    <div class="vertical-menu">
      <a href="/admin/home/users" class="@yield('homeactive') homecolor">Home</a>
      <a href="/admin/group" class="@yield('groupactive')">group</a>
      <a href="/admin/notice/all" class="@yield('noticeactive')">Notice</a>
    </div>
  </div>
  <div class="box-2">
    <ul>
      <li><a class="active" href="{{route('logout.post')}}">LOGOUT</a></li>
      <li><a href="#news">News</a></li>
      <li><a href="#contact">Contact</a></li>
      <li><a href="/">sd</a></li>
    </ul>
    <div class="dashboard">
      @section('contain')

      @show
    </div>
  </div>
</div>
</body>
</html>