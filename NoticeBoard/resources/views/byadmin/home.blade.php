@extends('byadmin.index')
@section('title','Home')
@section('contain')


<div style="margin-top:2%;float:right;margin-right:2%;">
<button><a href="registernormaluser">Add Normal User</a></button>
<button><a href="registeradminuser">Add Admin User</a></button>
<button ><a href="{{route('hometrash')}}">trash</a></button>
</div>
<span>	    
    @if(Session::has('report'))
            <span>{{Session::get('report')}}</span>
        @endif
      </span>
<div class="table">
  <table class="product">
    <tr>
      <th>name</th>
      <th>Post</th>
      <th>email</th>  
      <th>remarks</th>
    </tr>
     @foreach($users as $u)
    <tr>
    	<td>{{$u->first_name}}</td>
      <td>@if ($u->isadmin==0)
        Admin
      @else
        User
      @endif</td>
      <td>{{$u->email}}</td> 
      <td>
      </a>&nbsp;&nbsp;&nbsp;<a style="color:green" href="edit/{{$u->id}}">edit
      </a>&nbsp;&nbsp;&nbsp;<a style="color:red" href="delete/{{$u->id}}">delete</a>&nbsp;&nbsp;</td>
    </tr>
      @endforeach
  </table>
</div>



@endsection
@section('homeactive','active')

