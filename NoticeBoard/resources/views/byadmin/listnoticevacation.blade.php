@extends('byadmin.index')
@section('title','Notice')
@section('noticeactive','active')
@section('contain')

<div style="margin-top:2%;">
<br>
<div>
<button><a href="all">ALL Notice</a></button>
<button><a href="exam">Exam Notice</a></button>
<button style="background-color:green;"><a style="color:white;" href="vacation">Vacation Notice</a></button>
<button><a href="eca">ECA Notice</a></button>
<button style="float:right;margin-right:5%;"><a href="addnotice">Add Notice</a></button>
</div>
<div class="table">
  <table class="product">
    <tr>
      <th>Title</th>
      <th>Class</th>
      <th>Details</th>
      <th>Remarks</th>
       
    </tr>
    @foreach($result as $g)
    <tr>
      <td>{{$g->groups->group_name}}</td>
      <td>{{$g->class}}</td> 
      <td>{{$g->details}}</td>
      <td>
      </a>&nbsp;&nbsp;&nbsp;<a style="color:green" href="edit/{{$g->notice_id}}">edit
      </a>&nbsp;&nbsp;&nbsp;<a style="color:red" href="delete/{{$g->notice_id}}">delete</a>&nbsp;&nbsp;</td>
    </tr>
      @endforeach
  </table>
</div>
@endsection