@extends('byadmin.index')
@section('title','Update')
@section('homeactive','active')
@section('contain')
    <div ><center><h1> Update User </h1></center></div>
    <div>
        
        <center><form action="{{route('updatehomeuser')}}" method="post">
            @csrf
       
            <input type="hidden" name="isadmin" value="{{$edit->isadmin}}"><br>
            First Name:
            <input type="text" name="name" value="{{$edit->first_name}}"><br>
            Email:
            <input type="text" name="email" value="{{$edit->email}}"><br>
            Password:
            <input type="password" name="password" value="{{$edit->password}}" ><br><br>

            <button type="submit">submit</button>
            
        </form></center>
    </div>
@endsection