@extends('byadmin.index')
@section('title','Registrations')
@section('homeactive','active')
@section('contain')
<div style="margin-top:2%;float:right;margin-right:5%;">
<button><a href="registernormaluser">Add Normal User</a></button>
<button style="background-color:green;"><a style="color:white;" href="registeradminuser">Add Admin User</a></button>
</div>
    <div ><center><h1> Normal User registrations </h1></center></div>
    <div>
        
        <center><form action="{{ route('normalregister') }}" method="post">
            @csrf
            <input type="hidden" name="isadmin" value="1"><br>
            Name:
            <input type="text" name="name"><br>
            Email:
            <input type="text" name="email" required autocomplete="email"><br>
            Password:
            <input type="password" name="password" required autocomplete="new-password"><br><br>
            <button type="submit">submit</button>
            
        </form></center>
    </div>
@endsection